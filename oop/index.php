<?php 
    require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new Animal("shaun");

    echo "The sheep name: " . $sheep->get_name();
    echo "<br />";
    echo "The sheep legs: " . $sheep->get_legs();
    echo "<br />";
    echo "Is cold blooded: " . $sheep->get_cold_blooded();

    echo "<br />";
    echo "<br />";

    $sunkogong = new Ape("kera sakti");

    echo "The  name: " . $sunkogong->get_name();
    echo "<br />";
    echo "The  legs: " . $sunkogong->get_legs();
    echo "<br />";
    echo "Is cold blooded: " . $sunkogong->get_cold_blooded();
    echo "<br />";
    echo "Yell: " . $sunkogong->yell();


    
    echo "<br />";
    echo "<br />";

    $kodok = new Frog("buduk");

    echo "The  name: " . $kodok->get_name();
    echo "<br />";
    echo "The  legs: " . $kodok->get_legs();
    echo "<br />";
    echo "Is cold blooded: " . $kodok->get_cold_blooded();
    echo "<br />";
    echo "Jump: " . $kodok->jump();

    
?>